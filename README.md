# argocd

```
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v2.7.11/manifests/install.yaml
```

# argocd-apps

```
kubectl apply -f argo/projects/
kubectl apply -f argo/apps/ -n argocd
```